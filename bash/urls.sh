#!/bin/bash

set -e

URLS_FILE="${1:-urls.txt}"

get_url() {
    if [[ ! $1 =~ (http|https)://* ]]
    then
        echo "Given string $1 is not an url"
        exit 1
    fi
    response=$(curl --write-out %{http_code} --silent --show-error --fail --output /dev/null $1)
    if [[ $response == 4* ]] || [[ $response == 5* ]] || [[ $response == 0* ]] ;
    then
        echo "Status of $1 is $response"
        exit 1
    fi
    echo $1 $response
}

if [[ ! -f $URLS_FILE ]] ; then
    echo "File $URLS_FILE does not exists"
    exit 1
fi

while IFS= read -r line; do
    get_url $line
done < $URLS_FILE

