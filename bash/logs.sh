#!/bin/bash

LOG_FILE="${1:-logs.txt}"

if [[ ! -f $LOG_FILE ]] ; then
    echo "File $LOG_FILE does not exists"
    exit 1
fi


tail -1 $LOG_FILE | awk '{print $1}' | sort -nk1 | uniq -c | sort -nk1